import {Component, Injectable, Input, SimpleChange} from '@angular/core';
import { Observable }     from 'rxjs/Observable';
import { Http, Response, URLSearchParams} from '@angular/http';

@Component({
    selector: 'my-app',
    // template: '<h1>My First Angular 2 App</h1>'
    templateUrl: 'app/myapp.component.html'
})
@Injectable()
export class AppComponent {
  constructor (private http: Http) {}
  products:any[] = [];
  username:string = '';
  customers:any[] = [];
  focused:boolean = false;
  searchText:string = '';
  changed:boolean = false;

  ngOnInit(){
    this.getCustomers();
  }

  setName(idx:number){
    console.log('idx: ' + idx);
    if (this.username !== this.customers[idx].Customer_Name){
      this.username = this.customers[idx].Customer_Name;
      this.changed = true;
    }
  }

  clearUser(){
    if (this.username !== ''){
      this.changed = true;
      this.username = '';
    }
  }

  getCustomers(){
    console.log('getCustomers');
    let customerUrl = 'http://localhost:10010/v1/search/customers';


    this.http.get(customerUrl).subscribe(
      data => {
        this.customers = data.json().docs;
      },
      err => console.error(err)
    );
  }

  doSearch(){
    var that = this;
    return new Promise(function(resolve, reject){
      console.log('username: ' + that.username);
      console.log('that.searchText: ' + that.searchText);
      let searchUrl = 'http://localhost:10010/v1/search/products';

      if (that.searchText.length > 1){
        searchUrl += '?productName=' + encodeURI(that.searchText);
        if (!!that.username){
          searchUrl += '&customerName=' + encodeURI(that.username);
        }

        console.log('doing search on "' + searchUrl + '"');
        that.http.get(searchUrl).subscribe(
          data => {
            that.products = data.json().docs;
            that.changed=false;
            resolve();
          },
          err => {
            console.error(err);
            that.changed=false;
            reject();
          }
        );
      }else{
        that.products = [];
        that.changed=false;
        resolve();
      }
    });
  }

  showProducts(){
    return this.products.length > 0 && this.focused ;
  }

  onBlur(){
    this.focused = false;
  }

  onChange(){
    this.changed = true;
    console.log('changed: ' + this.changed);
  }

  onFocus(){
    var that = this;
    if (this.changed){
      console.log('doing new search on focus');
      this.doSearch().then(function(){
        that.focused = true;
      });
    }else{
      console.log('keeping old search results on focus');
      that.focused = true;
    }
  }


  private extractData(res: Response) {
    let body = res.json();
    return body.data || { };
  }
  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = error.message || error.statusText || 'Server error';
    // console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
}


